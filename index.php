<?php
header("Content-Type: application/json");

$app = new App();

$result = $app->handle();

print $result;
die();


final class App
{
    public function handle()
    {

        try {
            $inputString = $this->getInputString();
            $uniqueFromRequest = $this->uniqueFromRequest($inputString);
            $data = $this->getData($inputString, $uniqueFromRequest);

            return json_encode($data, JSON_UNESCAPED_UNICODE);

        } catch (Exception $e) {
            return json_encode([
                'error' => $e->getMessage()
            ], JSON_UNESCAPED_UNICODE);
        }
    }

    private function getData($inputString, $uniqueFromRequest)
    {
        $preFinalArray = array();

        foreach ($inputString as $key => $value) {
            $preFinalArray[] = Utils::uniqueCombination($uniqueFromRequest[$key]['products'], $value);
        }

        $finalProducts = Utils::recursiveMerge($preFinalArray);

        $result = array();

        foreach ($finalProducts as $item) {

            $price = 0;
            $products = array();

            foreach ($item as $v) {
                for ($i = 0; $i < count($v); $i++) {
                    $products[] = [
                        'type' => $v[$i]['type'],
                        'value' => $v[$i]['value']
                    ];
                    $price = $price + intval($v[0]['price']);
                }
            }

            $result[] = [
                'products' => $products,
                'price' => $price
            ];
        }

        return $result;
    }

    private function getInputString()
    {
        $string = $_GET['string'];
        if (!$string) {
            throw new Exception('Нет входящей строки');
        }

        $filteredString = preg_replace('/[^a-z]/', '', strtolower($string));
        $characters = str_split($filteredString);

        $db = Database::getInstance();
        $codes = $db->getConnection()->query('SELECT DISTINCT code FROM ingredient_type');

        $codesFromDb = array();

        if ($codes->rowCount() > 0) {
            // Вывод результатов
            foreach ($codes as $code) {
                $codesFromDb[] = $code['code'];
            }
        } else {
            throw new Exception('В базе нет данных');
        }

        $finalCodes = array_intersect($characters, $codesFromDb);
        $uniqueFromRequest = array_count_values($finalCodes);

        return $uniqueFromRequest;

    }


    private function uniqueFromRequest($uniqueFromRequest)
    {

        $result = array();
        $db = Database::getInstance();

        foreach ($uniqueFromRequest as $key => $value) {

            $res = $db->getConnection()->query("
SELECT it.title as type, i.title as value, i.price, it.code as code FROM ingredient i
left join ingredient_type it on it.id = i.type_id
WHERE it.code LIKE '{$key}'
 ");

            $uniqueValues = $res->fetchAll();


            if (count($uniqueValues) > 0) {
                $result[$key]['count'] = count($uniqueValues);
                foreach ($uniqueValues as $uniqueValue) {
                    $result[$key]['products'][] = $uniqueValue;
                }

                $result[$key]['type'] = $uniqueValues[0]['type'];
                $result[$key]['code'] = $uniqueValues[0]['code'];

            } else {
                throw new Exception('0 результатов');
            }

        }

        foreach ($uniqueFromRequest as $key => $value) {
            if ($result[$key]['count'] < $value) {
                throw new Exception("В базе недостаточно начинки типа '{$result[$key]['type']}' под ваш запрос");
            }
        }

        return $result;

    }

}

class Database
{
    private static $instance;
    private $connection;

    private function __construct()
    {
        $this->connection = new PDO('mysql:host=127.0.0.1;dbname=zh', 'root', 'change-me');
    }


    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getConnection()
    {
        return $this->connection;
    }
}


class Utils
{
    static function uniqueCombination($in, $length = 0)
    {
        $count = count($in);
        $variants = pow(2, $count);

        $result = array();
        for ($i = 0; $i < $variants; $i++) {

            $b = sprintf("%0" . $count . "b", $i);
            $out = array();

            for ($j = 0; $j < $count; $j++) {
                if ($b[$j] == '1') {
                    $out[] = $in[$j];
                }
            }
            count($out) == $length and $result[] = $out;
        }
        return $result;
    }


    static function recursiveMerge($input)
    {
        $result = array();

        foreach($input as $key=>$values) {
            if (empty($values)) {
                continue;
            }

            if (empty($result)) {
                foreach ($values as $value) {
                    $result[] = array($key => $value);
                }
            } else {
                $append = array();

                foreach ($result as &$product) {
                    // $product указываем в фориче с ссылкой, чтобы сохранить данные при шифте
                    // и в первом массиве у нас была полная выборка
// дёргаем значение и уменьшаем их количество, чтобы перебирать их форичем
                    $product[$key] = array_shift($values);

                    // делаем копию массива
                    $copy = $product;

// пока есть значения формируем массив продукта
                    foreach ($values as $item) {
                        $copy[$key] = $item;
                        $append[] = $copy;
                    }

// закидываем полный цикл прохода в один продукт
                    array_unshift($values, $product[$key]);
                }

// закидываем в финал проход
                $result = array_merge($result, $append);
            }
        }

        return $result;
    }
}